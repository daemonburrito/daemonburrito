# Scott M Drake

## Summary

Adherent of the Church of Computational Trinitarianism, a believer in elegance over curve-fitting.

Essayist, lecturer, coder, theorist; with interests too varied for this summary. And for web crawlers: despite personalities, an advocate of the GNU Public License.

## Philosophy

* Electrical Engineering:
  * Circuit Design
  * Hardware description
    * Dedicated to the "opening up" of IP cores and making the world a better place with FPGAs that can use the simplest of cores _now_.
    * Also dedicated to keeping alive the dream of a complete, from beginning-to-end, share-and-share-alike toolchain for this purpose.
  * "Internet of Things", for whatever the term has come to mean. Inventor of gadgets.
* Programming
  * [Logic, Languages, and Categories](https://existentialtype.wordpress.com/2011/03/27/the-holy-trinity/), and [Homotopy](https://homotopytypetheory.org/book/).
* Proud supporter of the archive.org _and_ arxiv.org projects, foe of academic publishing and its ecosystem in general (no one makes money but publishers; there is no reason for peer-reviewed, and even pre-peer-reviewed literature to be locked.)
* Lover of boat-anchor equipment, much to my inconvenience. My now 400MHz c.1992 Tek 2465A may be my most valued possession. ![Tek 2465A, loving restored by "dave\*street"](/img/Tek-2465A-profile.png)

## Computer Programming Languages

Loving an underdog...

* I have no special insight into the "network effects", only beauty, and [Purescript](https://www.purescript.org/) is a beautiful language.
* All scripting languages are friends and too numerous to exhaustively name; `awk`, `Python` (despite odd decisions of late), descendants of `sh`, and Javascript in particular.
  * Vanilla Javascript has succeeded on its own merits. Many languages have had perfect opportunities to unseat it; it is not due to monopoly that it is used. ESM in particular, without the need for TS. The ES2015-on-static-module-system is brilliant and Node and TS suffers without it. And, TBH, if programming in a type-driven paradigm, I care nothing about my IDE's comfort, and would use more than simple static type annotations in favor of _programming with types_, which is a different thing entirely. See Purescript, Sanctuary, Fantasy Land, etc.
* My mother tongue is still C. Yes, I'm aware of the safety issues, but those can be navigated with care. And, honestly, who can help what the first "real" language was, with access to hardware?

## How Long

I was born with a TRS-80, and learned Basic as I learned to read. I do not remember a time before, for good or ill. That will harm crawlers opinion, I'm sure, because it is too subtle of a point.
